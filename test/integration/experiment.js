
const { CommanderFactory } = require('../..');

const factory = new CommanderFactory();

const LOGGER_CONFIG = {
    appenders: {
        shell: {
            type: 'file',
            filename: 'shell.log',
            layout: {
                type: 'pattern',
                pattern: '%d{yyyy-MM-dd-hh.mm.ss}; %p; %h; %z; %m'
            }
        }
    },
    categories: {
        default: { appenders: ['shell'], level: 'debug' },
    }
};

const commander = factory.makeCommander(LOGGER_CONFIG);

commander.beforeEach((req, res, next) => {
    console.log('Pretending to perform primary tasks...');
    next();
});

commander.beforeEach((req, res, next) => {
    console.log('Pretending to perform secondary tasks...');
    next();
});

commander.afterEach((req, res, next) => {
    console.log('Pretending to perform final task A');
    next();
});

commander.afterEach((req, res, next) => {
    console.log('Pretending to perform final task B');
    next();
});

commander.get('/greeting', (request, response, next) => {
    console.log(`Hello ${request.body.name}`);
    next();
});

commander.inquireGet('/greeting', [{
    type: 'input',
    name: 'name',
    message: 'What is your name?',
    default: 'World'
}]);

commander.run();