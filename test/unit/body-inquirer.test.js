const chai = require('chai');
const { expect } = chai;

const sinon = require('sinon');
chai.use(require('sinon-chai'));

const { BodyInquirer } = require('../..');

const sandbox = sinon.createSandbox();

const sequenceFactory = { newSequence: sandbox.stub() };

const sequence = { accepts: sandbox.stub(), inquireBody: sandbox.stub() };

const next = sandbox.stub();

describe('BodyInquirer', () => {
    afterEach(() => sandbox.reset());

    describe('#inquire', () => {
        it('Should return an inquiry function if the dispatcher is found', () => {
            const subject = new BodyInquirer({ }, [ ], [
                { dispatcher: 'dispatcher', pathTemplate: '/path' }
            ]);
            const inquiry = subject.inquire('dispatcher');
            expect(inquiry).to.be.instanceOf(Function);
        });
        it('Should return null if the dispatcher is not found', () => {
            const subject = new BodyInquirer({ });
            const inquiry = subject.inquire('dispatcher');
            expect(inquiry).to.be.null;
        });
    });

    describe('#makeInquiry', () => {
        it('Should attach a body to the request if there is a corresponding sequence', async () => {
            sequence.accepts.returns(true);
            sequence.inquireBody.resolves('body');
            const subject = new BodyInquirer({ }, [ sequence ], [ ]);
            const inquiry = subject.makeInquiry('/path');
            await inquiry({ method: 'GET' }, { }, next);
            expect(sequence.accepts).to.be.calledOnceWithExactly('GET', '/path');
            expect(sequence.inquireBody).to.be.calledOnceWithExactly();
        });
        it('Should do nothing if there is no corresponding sequence', async () => {
            sequence.accepts.returns(false);
            const subject = new BodyInquirer({ }, [ sequence ], [ ]);
            const inquiry = subject.makeInquiry('/path');
            await inquiry({ method: 'GET' }, { }, next);
            expect(sequence.accepts).to.be.calledOnceWithExactly('GET', '/path');
            expect(sequence.inquireBody).to.not.be.called;
        });
    });

    describe('#registerPathTemplate', () => {
        it('Should collect a path template and its corresponding dispatcher', () => {
            const records = [ ];
            const subject = new BodyInquirer({ }, [ ], records);
            subject.registerPathTemplate('/path', 'dispatcher');
            expect(records).to.be.deep.equal([
                { pathTemplate: '/path', dispatcher: 'dispatcher' }
            ]);
        });
    });

    describe('#delete', () => {
        it('Should instantiate an inquiry sequence', () => {
            sequenceFactory.newSequence.returns('s');
            const sequences = [ ];
            const subject = new BodyInquirer(sequenceFactory, sequences);
            subject.delete('/path', 'questions');
            expect(sequences).to.be.deep.equal([ 's' ]);
            expect(sequenceFactory.newSequence)
                .to.be.calledOnceWithExactly('delete', '/path', 'questions');
        });
    });

    describe('#post', () => {
        it('Should instantiate an inquiry sequence', () => {
            sequenceFactory.newSequence.returns('s');
            const sequences = [ ];
            const subject = new BodyInquirer(sequenceFactory, sequences);
            subject.post('/path', 'questions');
            expect(sequences).to.be.deep.equal([ 's' ]);
            expect(sequenceFactory.newSequence)
                .to.be.calledOnceWithExactly('post', '/path', 'questions');
        });
    });

    describe('#put', () => {
        it('Should instantiate an inquiry sequence', () => {
            sequenceFactory.newSequence.returns('s');
            const sequences = [ ];
            const subject = new BodyInquirer(sequenceFactory, sequences);
            subject.put('/path', 'questions');
            expect(sequences).to.be.deep.equal([ 's' ]);
            expect(sequenceFactory.newSequence)
                .to.be.calledOnceWithExactly('put', '/path', 'questions');
        });
    });

    describe('#get', () => {
        it('Should instantiate an inquiry sequence', () => {
            sequenceFactory.newSequence.returns('s');
            const sequences = [ ];
            const subject = new BodyInquirer(sequenceFactory, sequences);
            subject.get('/path', 'questions');
            expect(sequences).to.be.deep.equal([ 's' ]);
            expect(sequenceFactory.newSequence)
                .to.be.calledOnceWithExactly('get', '/path', 'questions');
        });
    });
});