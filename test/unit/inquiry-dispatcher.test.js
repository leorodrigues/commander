const chai = require('chai');
const { expect } = chai;

const sinon = require('sinon');
chai.use(require('sinon-chai'));

const { InquiryDispatcher } = require('../..');

const sandbox = sinon.createSandbox();

const delegate = { accepts: sandbox.stub(), dispatch: sandbox.stub() };

const bodyInquirer = { inquire: sandbox.stub() };

describe('InquiryDispatcher', () => {
    afterEach(() => sandbox.reset());

    describe('#accepts', () => {
        it('Should return from the delegate', () => {
            delegate.accepts.returns('yes');
            const subject = new InquiryDispatcher(bodyInquirer,  delegate);
            expect(subject.accepts('a', 'b')).to.be.equal('yes');
            expect(delegate.accepts).to.be.calledOnceWithExactly('a', 'b');
        });
    });

    describe('#dispatch', () => {
        describe('If no pre handlers were given', () => {
            it('Should place an inquiry if there is one', async () => {
                delegate.dispatch.resolves('ok');
                bodyInquirer.inquire.returns('I');
                const subject = new InquiryDispatcher(bodyInquirer,  delegate);
                expect(await subject.dispatch('p'))
                    .to.be.equal('ok');
                expect(bodyInquirer.inquire)
                    .to.be.calledOnceWithExactly(subject);
                expect(delegate.dispatch)
                    .to.be.calledOnceWithExactly('p', ['I'], undefined);
            });
            it('Should simply dispatch via delegate if there is no inquiry', async () => {
                delegate.dispatch.resolves('ok');
                bodyInquirer.inquire.returns(null);
                const subject = new InquiryDispatcher(bodyInquirer,  delegate);
                expect(await subject.dispatch('p'))
                    .to.be.equal('ok');
                expect(bodyInquirer.inquire)
                    .to.be.calledOnceWithExactly(subject);
                expect(delegate.dispatch)
                    .to.be.calledOnceWithExactly('p', undefined, undefined);
            });
        });
        describe('If pre handlers were given', () => {
            it('Should place an inquiry if there is one', async () => {
                delegate.dispatch.resolves('ok');
                bodyInquirer.inquire.returns('I');
                const subject = new InquiryDispatcher(bodyInquirer,  delegate);
                expect(await subject.dispatch('p', ['a']))
                    .to.be.equal('ok');
                expect(bodyInquirer.inquire)
                    .to.be.calledOnceWithExactly(subject);
                expect(delegate.dispatch)
                    .to.be.calledOnceWithExactly('p', ['I', 'a'], undefined);
            });
            it('Should simply dispatch via delegate if there is no inquiry', async () => {
                delegate.dispatch.resolves('ok');
                bodyInquirer.inquire.returns(null);
                const subject = new InquiryDispatcher(bodyInquirer,  delegate);
                expect(await subject.dispatch('p', ['a']))
                    .to.be.equal('ok');
                expect(bodyInquirer.inquire)
                    .to.be.calledOnceWithExactly(subject);
                expect(delegate.dispatch)
                    .to.be.calledOnceWithExactly('p', ['a'], undefined);
            });
        });
    });
});