const chai = require('chai');
const { expect } = chai;

const sinon = require('sinon');
chai.use(require('sinon-chai'));

const { PromptAction, InterruptCommanderError } = require('../../');

const sandbox = sinon.createSandbox();

const router = {
    dispatch: sandbox.stub(),
};

const logger = {
    info: sandbox.stub(),
    error: sandbox.stub()
};

const presentPrompt = sandbox.stub();

describe('PromptAction', () => {
    afterEach(() => sandbox.reset());

    describe('#fire', () => {
        it('Should execute a prompt/dispatch cycle', async () => {
            presentPrompt.returns({ cmdLine: 'fake line' });
            router.dispatch.returns('yes');
            const subject = new PromptAction(router, presentPrompt, logger);
            expect(await subject.fire('!')).to.be.equal('yes');
            expect(presentPrompt).to.be.calledOnceWithExactly('!');
            expect(router.dispatch).to.be.calledOnceWithExactly('fake line');
        });
    });

    describe('#handleError', () => {
        it('Should signal a continuation if "error" is a regular error', async () => {
            const subject = new PromptAction(router, presentPrompt, logger);
            expect(await subject.handleError(new InterruptCommanderError('fake error'))).to.be.true;
        });

        it('Should signal an interruption if "error" is an interrupt error', async () => {
            const subject = new PromptAction(router, presentPrompt, logger);
            expect(await subject.handleError(new Error('fake error'))).to.be.false;
        });
    });
});