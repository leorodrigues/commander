const chai = require('chai');
const { expect } = chai;

const sinon = require('sinon');
chai.use(require('sinon-chai'));

const { InquiryRoutingKit, InquiryDispatcher } = require('../..');

const sandbox = sinon.createSandbox();

const bodyInquirer = { registerPathTemplate: sandbox.stub() };

const delegateKit = { newDispatcher: sandbox.stub() };

describe('InquiryRoutingKit', () => {
    afterEach(() => sandbox.reset());

    describe('#newDispatcher', () => {
        it('Should fabricate a new dispatcher', () => {
            delegateKit.newDispatcher.returns('fake dispatcher');
            const subject = new InquiryRoutingKit(bodyInquirer, delegateKit);
            subject.newDispatcher('GET', '/customer', 'fake handler array');
            expect(delegateKit.newDispatcher)
                .to.be.calledOnceWithExactly('GET', '/customer', 'fake handler array');
            expect(bodyInquirer.registerPathTemplate)
                .to.be.calledOnceWithExactly('/customer', sinon.match(o =>
                    o instanceof InquiryDispatcher));
        });
    });
});