const chai = require('chai');
const { expect } = chai;

const sinon = require('sinon');
chai.use(require('sinon-chai'));

const { InquirySequence } = require('../..');

const sandbox = sinon.createSandbox();

const prompt = sandbox.stub();

describe('InquirySequence', () => {
    describe('#accepts', () => {
        it('Should accept if both method and path match', () => {
            const subject = new InquirySequence(prompt, 'get', '/path', 'q');
            expect(subject.accepts('get', '/path')).to.be.true;
        });
        it('Should reject if method is different', () => {
            const subject = new InquirySequence(prompt, 'get', '/path', 'q');
            expect(subject.accepts('put', '/path')).to.be.false;
        });
        it('Should reject if path is different', () => {
            const subject = new InquirySequence(prompt, 'get', '/path', 'q');
            expect(subject.accepts('get', '/customer')).to.be.false;
        });
    });
    describe('#inquireBody', () => {
        it('Should ask the user for input', async () => {
            prompt.resolves('answers');
            const subject = new InquirySequence(prompt, 'get', '/path', 'q');
            expect(await subject.inquireBody()).to.be.equal('answers');
            expect(prompt).to.be.calledOnceWithExactly('q');
        });
    });
});