
class InquiryDispatcher {
    constructor(bodyInquirer, delegate) {
        this.delegate = delegate;
        this.bodyInquirer = bodyInquirer;
    }

    accepts(method, path) {
        return this.delegate.accepts(method, path);
    }

    async dispatch(path, preHandlers, postHandlers) {
        const inquiry = this.bodyInquirer.inquire(this);
        if (inquiry)
            preHandlers = preHandlers ? [inquiry, ...preHandlers] : [inquiry];
        return await this.delegate.dispatch(path, preHandlers, postHandlers);
    }
}

module.exports = InquiryDispatcher;