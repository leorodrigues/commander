
class BodyInquirer {

    constructor(sequenceFactory, sequences, pathTemplateRecords) {
        this.sequenceFactory = sequenceFactory;

        /* istanbul ignore next */
        this.sequences = sequences || [ ];

        /* istanbul ignore next */
        this.pathTemplateRecords = pathTemplateRecords || [ ];
    }

    delete(path, questions) {
        this.sequences.push(
            this.sequenceFactory.newSequence('delete', path, questions));
    }

    post(path, questions) {
        this.sequences.push(
            this.sequenceFactory.newSequence('post', path, questions));
    }

    put(path, questions) {
        this.sequences.push(
            this.sequenceFactory.newSequence('put', path, questions));
    }

    get(path, questions) {
        this.sequences.push(
            this.sequenceFactory.newSequence('get', path, questions));
    }

    registerPathTemplate(pathTemplate, dispatcher) {
        this.pathTemplateRecords.push({ pathTemplate, dispatcher });
    }

    inquire(dispatcher) {
        const forDispatcher = r => r.dispatcher === dispatcher;
        const record = this.pathTemplateRecords.find(forDispatcher);
        return record ? this.makeInquiry(record.pathTemplate) : null;
    }

    makeInquiry(pathTemplate) {
        return async (request, response, next) => {
            const method = request.method;
            const accepting = s => s.accepts(method, pathTemplate);
            const sequence = this.sequences.find(accepting);
            if (sequence)
                request.body = await sequence.inquireBody();
            next();
        };
    }
}

module.exports = BodyInquirer;