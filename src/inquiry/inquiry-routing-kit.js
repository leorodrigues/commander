
const InquiryDispatcher = require('./inquiry-dispatcher');

class InquiryRoutingKit {
    constructor(bodyInquirer, delegateKit) {
        this.delegateKit = delegateKit;
        this.bodyInquirer = bodyInquirer;
    }

    /* istanbul ignore next */
    newResponse(context) {
        return this.delegateKit.newResponse(context);
    }

    /* istanbul ignore next */
    newRequest(context) {
        return this.delegateKit.newRequest(context);
    }

    /* istanbul ignore next */
    newContext(params, method, path) {
        return this.delegateKit.newContext(params, method, path);
    }

    newDispatcher(method, path, handlers) {
        const delegate = this.delegateKit.newDispatcher(method, path, handlers);
        const dispatcher = new InquiryDispatcher(this.bodyInquirer, delegate);
        this.bodyInquirer.registerPathTemplate(path, dispatcher);
        return dispatcher;
    }
}

module.exports = InquiryRoutingKit;
