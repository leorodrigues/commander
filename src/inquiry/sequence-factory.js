const IquirySequence = require('./inquiry-sequence');

/* istanbul ignore next */
class SequenceFactory {
    constructor(inquirerPrompt) {
        this.inquirerPrompt = inquirerPrompt;
    }

    newSequence(method, path, questions) {
        return new IquirySequence(this.inquirerPrompt, method, path, questions);
    }
}

module.exports = SequenceFactory;