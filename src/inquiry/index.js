module.exports = {
    BodyInquirer: require('./body-inquirer'),
    SequenceFactory: require('./sequence-factory'),
    InquirySequence: require('./inquiry-sequence'),
    InquiryDispatcher: require('./inquiry-dispatcher'),
    InquiryRoutingKit: require('./inquiry-routing-kit')
};