
class InquirySequence {
    constructor(inquirerPrompt, method, path, questions) {
        Object.assign(this, { method, path, questions, inquirerPrompt });
    }

    accepts(method, path) {
        return this.method === method && this.path === path;
    }

    async inquireBody() {
        return await this.inquirerPrompt(this.questions);
    }
}

module.exports = InquirySequence;