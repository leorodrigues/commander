
const DEFAULT_MESSAGE = 'Prompt cycle halted';

/* istanbul ignore next */
module.exports = class InterruptCommanderError extends Error {
    constructor(message = DEFAULT_MESSAGE) {
        super(message);
    }

    describe() {
        return this.message;
    }
};
