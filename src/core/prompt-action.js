const InterruptCommanderError = require('./interrupt-commander-error');

class PromptAction {
    constructor(router, presentPrompt, logger) {
        this.presentPrompt = presentPrompt;
        this.router = router;
        this.logger = logger;
    }

    async fire(prompt) {

        /* istanbul ignore next */
        prompt = prompt || COMMAND_PROMPT;

        const { cmdLine } = await this.presentPrompt(prompt);
        const interrupt = await this.router.dispatch(cmdLine);
        /* istanbul ignore next */
        if (interrupt)
            this.logger.info('Prompt cycle terminated.');
        return interrupt;
    }

    async handleError(error) {
        if (error instanceof InterruptCommanderError) {
            this.logger.error(error.describe());
            return true;
        }
        this.logger.error(error);
        return false;
    }

    /* istanbul ignore next */
    static get COMMAND_PROMPT() {
        return Object.assign({ }, COMMAND_PROMPT);
    }
}

const COMMAND_PROMPT = {
    'type': 'command',
    'name': 'cmdLine',
    'context': 0,
    'short': false,
    'message': ':'
};

module.exports = PromptAction;