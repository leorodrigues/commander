const log4js = require('log4js');
const inquirer = require('inquirer');
const inquirerCommandPrompt = require('inquirer-command-prompt');
const inquirerDatePickerPrompt = require('inquirer-datepicker-prompt');

const { Trampoline } = require('@leorodrigues/trampoline');
const { Router, RoutingKit } = require('@leorodrigues/request-router');

const {
    BodyInquirer,
    SequenceFactory,
    InquiryRoutingKit
} = require('../inquiry');

const PromptAction = require('./prompt-action');
const Commander = require('./commander');

/* istanbul ignore next */
class CommanderKit {
    makeBodyInquirer(sequenceFactory) {
        return new BodyInquirer(sequenceFactory);
    }

    makeCommander(trampoline, router, bodyInquirer, logger) {
        return new Commander(trampoline, router, bodyInquirer, logger);
    }

    makeRouter(bodyInquirer) {
        const routingKit = new RoutingKit();
        return new Router(new InquiryRoutingKit(bodyInquirer, routingKit));
    }

    makeTrampoline(action) {
        return new Trampoline(action);
    }

    makePromptAction(router, presentPrompt, logger) {
        return new PromptAction(router, presentPrompt, logger);
    }

    makePresentPrompt() {
        const inquirerPrompt = inquirer.createPromptModule();
        inquirerPrompt.registerPrompt('command', inquirerCommandPrompt);
        inquirerPrompt.registerPrompt('datetime', inquirerDatePickerPrompt);
        return inquirerPrompt;
    }

    makeSequenceFactory(presentPrompt) {
        return new SequenceFactory(presentPrompt);
    }

    makeLogger(configuration) {
        log4js.configure(configuration);
        return log4js.getLogger();
    }

    static get INSTANCE() {
        return INSTANCE;
    }
}

const INSTANCE = new CommanderKit();

module.exports = CommanderKit;
