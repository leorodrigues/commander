/* istanbul ignore next */
class Commander {
    constructor(trampoline, router, bodyInquirer, logger) {
        this.bodyInquirer = bodyInquirer;
        this.trampoline = trampoline;
        this.router = router;
        this.logger = logger;
    }

    beforeEach(...handlers) {
        this.router.beforeEach(...handlers);
    }

    afterEach(...handlers) {
        this.router.afterEach(...handlers);
    }

    delete(path, ...handlers) {
        this.router.delete(path, ...handlers);
    }

    post(path, ...handlers) {
        this.router.post(path, ...handlers);
    }

    put(path, ...handlers) {
        this.router.put(path, ...handlers);
    }

    get(path, ...handlers) {
        this.router.get(path, ...handlers);
    }

    inquireDelete(path, questions) {
        this.bodyInquirer.delete(path, questions);
    }

    inquirePost(path, questions) {
        this.bodyInquirer.post(path, questions);
    }

    inquirePut(path, questions) {
        this.bodyInquirer.put(path, questions);
    }

    inquireGet(path, questions) {
        this.bodyInquirer.get(path, questions);
    }

    async run() {
        this.logger.info('Comencing prompt cycle');
        this.trampoline.spring();
    }
}

module.exports = Commander;