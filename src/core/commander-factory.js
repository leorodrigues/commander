const CommanderKit = require('./commander-kit');


/* istanbul ignore next */
module.exports = class CommanderFactory {
    constructor(commanderKit) {
        this.commanderKit = commanderKit || CommanderKit.INSTANCE;
    }

    makeCommander(loggerConfiguration) {
        const cmdrKit = this.commanderKit;

        const logger = cmdrKit.makeLogger(loggerConfiguration);
        logger.debug('Initializing');

        const prompt = cmdrKit.makePresentPrompt();
        logger.debug('Prompt instantiated');

        const sequenceFactory = cmdrKit.makeSequenceFactory(prompt);
        logger.debug('Sequence factory instantiated');

        const bodyInquirer = cmdrKit.makeBodyInquirer(sequenceFactory);
        logger.debug('Body inquirer instantiated');

        const router = cmdrKit.makeRouter(bodyInquirer);
        logger.debug('Router instantiated');

        const action = cmdrKit.makePromptAction(router, prompt, logger);
        logger.debug('Action instantiated');

        const trampoline = cmdrKit.makeTrampoline(action);
        logger.debug('Trampoline instantiated');

        const commander = cmdrKit.makeCommander(
            trampoline, router, bodyInquirer, logger);
        logger.debug('Commander instantiated');

        return commander;
    }
};