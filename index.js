module.exports = {
    ...require('./src/core'),
    ...require('./src/inquiry')
};